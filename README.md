# Systemy równoległe i rozproszone 

## Sposoby uruchamiania i działania systemu kolejkowego

PBS - Portable batch system - jest to ystem kolejkowy działający na komputerach obliczneiowych, zarządzający zasobami, rezerwujący i przedzialający je odpowiednim użytkownikom zgodnie z obowiązującymi zasadami. Pozwala na optymalne wykorzystanie mocy obliczeniowych i pamięci komputera

Kolejne zadania użytkowników zostają umieszczone w tzw. kolejce. Gdy zadeklarowane przez użytkowników zapotrzebowanie przekracza dostępne zasoby obliczeniowe, zadania są kolejno przydzielane do obliczeń, zgodnie z priorytetem wyznaczonym na podstawie podanego zapotrzebowania na zasoby, tj. czasu procesora, pamięci operacyjnej i dyskowej.

System działa na podstawie algorytmu SJF (ang. shortest job first). Użytkownik deklaruje ilość zasobów, które będą potrzebne do wykonania danego zadania.

MPI.PBS - jest to plik, który jest wrzucany do kolejki za pomocą odpowiedniego polecenia. System ten posiada domyślne ustawienia, które mogą być odpowiednio zmieniane. 

Polecenia kolejki:
- qsub - wstawianie zadania do kolejki
- qstat - sprawdzenie statusu zadania wstawionego do kolejki
- qdel - usuwanie zadania z kolejki
- qalter - zmiana parametrów zadania w kolejce
- tracejob - informacje na temat parametrów procesów

Statusy:
- E - Job is exiting after having run
- H - Job is held
- Q - Job is queued, eligible to run or be routed
- R - Job is running
- T - Job is in transition
- W - Job is waiting for its requested execution time to be reached
- S - Job is suspended

CzasProcesora = WallTime * LiczbaProcesorów

Flagi PBS (przykłady):

- -l nodes=4:ppn=2 - 4 nodes i 2 processor per node
- -l walltime=12:00:00 - Zasoby potrzebują 12 godzin _wall clock time_ dla wykonania zadania
- -o - plik wyjściowy dla zapisanych danych
- -j oe - Zapisanie stdout i stderr do jednego pliku
- -m - Wyslanie maila, w sytuacji gdy zadanie sie zaczelo/skonczylo/anulowano
- -V - Specyfikuje wszystkie zmienne środowiskowe, zostaną przekazne do PBS.

Ważny przykład wywołania polecenia kolejkującego: *qsub nazwaSkryotu*
Przykład użycia komendy, pokazującej charakterystyki wykorzystania poszczególnych węzłów: *pbsnodes -a*

## Wyklad II

### Wydajność a pamięć

Warunki wydajnego korzystania z pamięci:

- Czas - proces korzysta z danych lub instrukcji z których korzystał w przeszłości (np. pętle, stosy, lokalne zmienne).
- Przestrzeń - proces powinien odnosić się kolejno do najbliższych sąsiadów w przestrzeni adresów.
- Kolejność - najbardziej prwadopodobne w kolejnym kroku czasowym są następujące po sobie odniesienia.

### Analiza wydajności

Analizie *muszą* podlegać zarówno sprzętowe jak i programowe składowe systemu.

Testy mogą być dokonywanie w sposób:

- *statyczny*: odbywa się na etapie tworzenia programu (przed jego uruchomieniem). Sprawdza poprawność kodu pod kontem syntaktycznym i semantycznym
- *dynamiczny*: odbywa się po stworzeniu programu i jego uruchomieniu.

Najważniejsze dla systemów rozproszonych i równoległych są testy dynamiczne - przedstawianie wzrostu wydajności względem szeregowego działania programów.

### Parametric computing

Pierwsze podejście - Kod sekwencyjny + dużo procesorów = przyśpieszenie

Poprawka na prawo Amndhal'a - Kod sekwencyjny + nie tak dużo wydajnych procesorów = przyśpieszenie

W świecie realnym pierwsze podejście nie gwarantuje pełni liniowego wzrostu wydajności działania programów. Większa ilość procesorów nie zawsze przekłada się wzrost wydajności działania danej aplikacji. Aby uzyskać najlepsze przyśpieszenia, powinno ograniczać się liczbę procesorów. Jest to praktykowane ze względu na próbę zniwelowania problemów komunikacji międzyprocesorowej. Karty graficzne, ze względu na swoją specyficzną architekture nie podlegają podobnym problemom, ze względu na tym, że w nich nie istnieje pojęcie wykonywania kodu szeregowego - tam wszystko niejako z definicji jest wykonywane w sposób równoległy.

### I/O dominant applications

Z dwóch komputerów równoległych o tej samej zsumowanym wskaźniku wydajności CPU lepszą wydajność dla aplikacji z dominującymi operacjami wejścia/wyjścia będzie miał ten, który posiada wolniejsze procesory.

W praktyce z kolei, jeśli pętla obliczeniowa trwa minuty, a dane mogą zostać przesłane w ciągu sekund, to prawdopodobnie jest do dobry kandydat na program równoległy.

Operacje wejścia/wyjścia tworzą problemy ze względu na czas oczekiwania na dane zasoby, przez co niekoniecznie bardziej wydajny procesor będzie działał `szybciej` w stosunku do procesora mniej wydajnego. Bardziej wydajny procesor, po prostu będzie musiał dłużej czekać na obsługę nadchodzącego zdarzenia wejścia/wyjścia.


Należy uważać i myśleć, w jaki sposób powinno się zrównoleglać dany program - ze względu na to co chcemy osiągnąć i jakie ma być efekt końcowy. Nie zawsze więcej jest równe lepiej. 

## Wyklad III

###  Middleware

- W warstwowym modelu rozproszonego systemu równoległwego middleware stanowi podstawową różnicę pomiędzy komputerem szeregowym a równoległym.
- Stanowi on warstwę softwareową zapewniającą zunifkowany dostęp do wszystkich węzłów systemu.
- Jest też odpowiedzialny za udostępnianie zoasobów, zarządzanie obciążeniem oraz informowanie o błędach w poszczególnych składowych systemu.

Middleware daje możliwość do uruchomenia systemu na jednym komputerze, a działania w sposób rozproszony na wielu z nich. Ponadto musi zapewnić dostęp do zasobów, dzięki któremu każdy z tych systemów będzie działała prawidłowo.

Pożądane cechy warstwy middleware:
- Pojedynczy punkt dostępu
- Spójny system plików
- Ujednolicona kontrola systemu
- Brak silnie wyodrębnionych podsieci
- Wzajemna dostępność pamięci innymi słowy dostęp do danych
- System kontroli zadań
- Spójny UI jednolisty dla systemu

Powyższe cechy definują system middleware, który jest nazywany *Single System Image*.
Inne pożądane cechy warstwy middleware:
- Spójna identyfikacja procesów
- Możliwość powrotu do przerwanych zadań
- Migracja procesów

Biblioteki komunikacji równoległej:

Obecnie standardem jest biblioteka MPI (message passing interface). Rzadziej wykorzystywana jest jej poprzedniczka PVM (paraller virtual machine).

Alternatywy:
- Sockety
- HPF
- RPC
- RMI
- Cobra

PVM jest pierwszym systemem, który powstał do kontroli procesów w systemach rozproszonych. Służy do pisania programów w językach C oraz Fortran. Działa on na zasadzie wysyłania komunikatów, zarządzania procesami oraz ich synchronizacji.

W skład PVM wchodzą:
- program konsoli
- demony umożliwiające współdziałanie programów składających się na aplikacje rozproszone
- biblioteki funkcji

MPI jest standardem - nie konkretną implementacją. Posiada on kilka różniących się od siebie implementacji. Dwie najbardziej popularne to MPICH i LAM. MPI podobnie jak PVM może być wykorzystywany zarówno w superkomputerach jak i sieciach heterogenicznych.

### Komunikacja grupowa

Ten rodzaj komunikacji jest całkowicie oddzielona od komunikacji point-to-point. Wszystkie funkcje w komunikatorze muszą wywołać procedurę komunikacji zbiorowej.

W odróżnieniu od komunikacji point-to-point nie istnieją tagi, ale istnieje konieczność wypełnienia bufora odbiorczego

Synchornizacja:
MPI\_Barrier(Comm) - Wszystkie procesy muszą wywołać tę funkcję aby kontynuować swoje działanie.

Podstawowa funkcja:

int MPI\_Bcast(void \*buffer, int count, MPI\_Datatype datatype, int root, MPI\_Comm comm)
int MPI\_Scatter(void \*sendbuf, int sendcnt, MPI\_Datatype sendtype, void \*recvbuf, int recvcnt, MPI\_Datatype recvtype, int root, MPI\_Comm comm) - Rowny podzial danych
int MPI\_Gather(void \*sendbuf, int sendcnt, MPI\_Datatype sendtype, void \*recvbuf, int recvcount, MPI\_Datatype recvtype, int root, MPI\_Comm comm) - Zbieranie od wszystkich danych
int MPI\_Scaterv,\_Gatherv


## Wyklad IV

###  Tworzenie algorytmów równoległych

PCAM - Metoda tworzenia programów równoległych
Jedno z pytań na egz. magisterskim dotyczy modelu PCAM!

Kompozycja danych i ziarnistosc - im bardziej ziarniste dane tym gorzej! 
Czas kosztem pamięci a pamięć kosztem czasu.

Zasady dobrego podzialu:

1. Definiuj przynajmniej o rzad wiecej zadan niz masz dostepnych procesorow
1. Unikaj zbednych obliczeń i nadmiarowych danych
1. Staraj się aby zadania były porównywalnych rozmiarów
1. Zwiększenie problemu obliczeniowego powinno prowadzić do zwiększenia ilości zadań a nie ich rozmiaru
1. O ile to możliwe zawsze przygotowuj kilka alternatywnych sposobów podziału

Komunikacja lokalna - wymiana informacji nastepuje pomiedzy niewielka ilościa "lokalnych" procesów.
Komunikacja globalna - nie jest lokalna, ergo centralizacja i sekwencyjność

Zasady dobrej komunikacji:
1. _Wszystkie zadanie wymagają takiej samej ilości komunikacji_
1. Zadania komunikują się tylko z niewielką ilością siąsiadów
1. Komunikacje powinny móc zachodzić niezależnie od innych zerówno dla pojedynczych zadań jak i całego zagadnienia obliczeniowego


# Wyklad V

### Redukcje

- Redukcja wymiaru
- Zwiekszenie uziarnienia
- Grupowanie fragmentów drzew
- Łączenie węzłów

Zwiększa się uziarnienie aby zmniejszyć koszty komunikacji

Zasady:
1. Zmniejszenie kosztów komunikacji poprzez zwiększenie lokalności obliczeń
1. Ewnetualna konieczność zwiększenia ilości danych nie może wpływać na skalowalność problemu
1. Skalowalność powinna być zawsze zachowana
1. Zawsze powinna istnieć możliwość daleszej redukcji zadań
1. W przypadku zrównoleglania istenijącego kodu szeregowego należy uwzględniać koszty wytworzniea nowego kodu i w razie konieczności zastosować rozwiązania alternatywne (np. rozwiązania ultra gruboziarniste)

Mapowanie

Staramy się umieszczać zadania które mogą być wykonywane równocześnie na różnych węzłach zwiększając zrównoleglanie zadania

Staramy się dążyć do maksymalizowania ilości lokalnych komunikacji zmniejszając jej koszty

Podstawowe pojęcia:
- Load-balancing - równoważenie obciążenia (często dynamiczne)
- Task-scheduling - gdzie jakie zadanie będzie wykonane


# Wyklad VI

### Open multi processing (OpenMP, OMP)

OMP jest interfejsem programowania aplikacji wielowątkowych dla architektur wieloprocesowych z pamięcią dzieloną.

Cechuję się przenośnością i elastycznością. Jest również relatywnie prosty w użyciu.

W przypadku pracy w architekturach z pamięcią rozproszoną z reguły stosuje się OpenMP w połączeniu z MPI (lub innym interfejsem komunikacyjnym).

OpenMP bazuje na równoległości obliczęń uzyskanej dzięki istnieniu wielu wątków mających dostęp do pamięci dzielonej. Dzięki dużej elastyczności zrównoleglenie może zostać uzyskane zarówno w sposób niemal automatyczny jak i dający pełną kontrolę nad wątkiem. OpenMP działa według modelu fork-join.

Standardowo programy OMP używają tylu wątków ile rdzeni CPU jest dostępnych dla systemu. Można to łatwo zmienić w kodzie programu jak i z wykorzystaniem zmiennej środowiskowej.

Najprostszy program:
int main(int agc, char\* argv[])
{
	\#pragma omp parallel
	printf("Hello world.\n");
	return 0;
}

dyrektywa omp paraller określa obszar kodu, który zostanie rodzielony na wątki.

Najczęściej wykorzystywaną konstukcją jest jednak:

\#pragma omp parallel for

pozwalającą rozbić pętle na wiele wątków. Należy jednak pamiętać o atrybutach współdzielenia danych. Standardowo wszystkie dane są dzielone a zmienne prywatne wątków.

\#pragma omp parallel for private(j, i, k)

\#pragma omp flush(max)

\#pragma omp critical


### Profilowanie

Profilowanie to badanie programu pod kątem ilości wywołań poszczególnych funkcji a także czasu potrzebnego na wykonanie każdej z nich. Pozwala to przede wszystkim na lepszą organizacje pracy programisty poprzez skoncentrowanie jej w tych częściach programu, których optymalizacja jest najbardziej istotna.

Podstawowym narzędziem do profilowania jest program gprof. Ogólny schemat profilowania składa się z trzech punktów:

- Kompilacja z odpowiednimi opcjami.
- Wykonanie programu.
- Analiza zwróconego przez program pliku.

GCC wymaga przełącznika -pg. Warto dołączyć także przełącznik -g który powoduje wygenerowanie informacji dla debuggera.

Program należy wykonać w możliwie normalny sposób w warunkach zbliżonych do produkcyjnych. Profil zostanie zapisany w aktualnym katalogu roboczym do pliku gmon.out o ile program zakończył swoje działanie poprawnie. W przypadku błędu profil nie zostanie zapisany.

# Wyklad V

### Problem wydajności systemu

T = N + N^2 / P                  - Czesc zrownoleglona posiada staly wzrost problemy niezrownoległego

T = (N+N^2)/P + 100              - Dodatkowe koszta - stałe koszta - niezależne od czegokolwiek

T = (N+N^2)/P + 0.6P^2           - Dodatkowe koszta - zwiazne przez ilość procesów

Czes wykonania: różnica pomiędzy czasem rozpoczęcia pierwszego procesu a zakończeniem ostatniego.

Podstawą do dalszej optymalizacji powinny być wyniki eksperymentów. Dane powinny być zawsze gromadzone dla wielu eksperymentów i podane analizie statystycznej. Uzyskiwane wyniki mogą nie być powtarzalne ze względu na:

1. Niedeterministiczny algorytm
1. Nieprecezyjny pomiar czasu
1. Zmienny narzut czasu związany z rozpoczęciem i zakończeniem procesów
1. Zabieranie zasobów przez inne procesy
1. Nieregularność przesyłania informacji mogącą prowadzić do powstania zatorów
1. Losowe przydzielanie zasobów systemowych

### Branie rzeczy do kupy

Modularne podejście do tworzenia czegokolwiek złożonego opiera się na podziale problemu na w miarę niezależne możlwie uniwersalne elementy, które potem można łączyć w większe całości.

Coś co ma dobrze działać w sposób równoległy i rozproszony prawie zawsze jest tworzone w oparciu o system modułowy.

Niestety koncepcja ta pociąga za sobą odwlekane.

# Wyklad VI - Egzamin magisterski

1. Podstawowe właściwości systemów rozproszonych
	- Komunikacja między systemami a aplikacjami
	- Synchronizacja danych i procesów
	- Warstwowy model klastra
	- Single system image
	- Plusy i minusy rozpraszania
1. Systemy scentralizowane i rozproszone  (przykłady):
	- Jeden system vs. wiele systemów
	- "Komputer"
	- GPU (ale też multi-GPU)
	- Klaster
	- Bazy danych
1. Charakterystyka komputerów zgodnie z klasyfikacją Flynna Johnsona
	- SISD, SIMD, MISM, MIMD
	- GMSV, GMMP, DMSV, DMMP
	- Dodatkowo przykłady i właściwości
1. Zagadnienia sprzętowe: wieloprocesory a multikomputery, architektury sieci połączeń, systemy homo- i heterogeniczne
	- komputer zbudowany z wielu systemów i jeden system z wielona procesorami
	- dostęp do pamięci
	- topologie sieci i dlaczego gwiaździsta?
	- skąd się bierze heterogeniczność?
1. Zagadnienia sprzętowe dla wieloprocesorów: pamięć dzielona, architektura UMA i NUMA
	- Dostęp do pamięci dzielonej i jego konsekwencje
	- Unified Memory Access jako standard w x86 (mostek północy)
	- Wpływ NUMA na efektywność i jego konsekwencje (cpu binding)
1. Rozproszone systemy operacyjne: ściśle powiązane, luźno powiązane, sieciowe systemy operacyjne.
	- rola magistrali
	- procesor -> sieć -> pamięć vs procesor -> pamięć -> sieć
	- wykorzystanie protokołów sieciowych do świadczenia usług systemowych
1. Definicja i porównanie przetwarzania równoległego i rozproszonego.
	- Dostęp do pamięci i protokoły komunikacyjne
	- MPI vs OpenMP
	- Cuda/OpenCL vs multi-GPU
1. Parametry wydajności systemów równoległych i rozproszonych
	- Prawo Amdhala
	- Czas
	- Koszt
	- Przyśpieszenie
	- Efektywność
	- I inne...
1. Procesy wielordzeniowe, systemy SMP, systemy MPP oraz systemy dla przetwarzania rozporoszonego (przetwarzanie klastrowe, przetwarzanie gridowe)
	- pamięć dzielona i rozproszona
	- klastry (SSI, PBS, MPI itd.), grid (SGE)
1. Cechy charakterystyczne narzędzi do realizacji obliczeń równoległych i rozproszonych PVM i MPI, OpenMP.
	- proces vs watek
	- komunikacja
	- MPP vs SMP
	- pamięć
1. Cechy dekompozycji jako metody konstrukcji aplikacji równoległych
	- zasady efektywnego podziału
	- dekompozycja danych vs dekompozycja algorytmu
	- dekompozycja hybrydowa (dane, algorytm, wątki, procesy)
1. Fazy tworzenia aplikacji równoległych - ich podstawowe zasady oraz równoległość danych i kodu
	- model PCAM
1. Problematyka ziarnistości i skalowalności (przykłady)
	- dekompozycja drobno- i gruboziarnista (przetwarzanie obrazów)
	- skalowalność algorytmu (np. LU, algorytmy stochastyczne)
	- bruteforce
	- metoda różnic skończonych
1. Podstawowe mechanizmy komunikacji aplikacji rozproszonych (protokoły sieciowe, gniazdka, RPC, specjalizowane biblioteki)
	- TCP/IP/UDP
	- domena i typ gniazdek
	- MPI i komunikacja synchroniczna, asynchroniczna i grupowa
	- RPC i jego zastosowania
1. Standardy COM/DCOM, WCF i COBRA
	- A jak to wszystko widzi Microsoft: Distributed Component Object Model
	- D... -> garbage collector, marshaling
	- Windows Communication Foundation - dane jako asynchroniczny strumień w SOA
	- Common Object Request Broker Architecture - dlaczego miała swoje 5 minut komunikacja między oprogramowaniem napisanym w róznych językach na różnych komputerach
